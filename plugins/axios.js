import Vue from 'vue';
import Cookie from 'js-cookie';

export default function ({ $axios, redirect, app }) {

    $axios.onRequest(config => {
      console.log('Making request to ' + config.url)
      console.log("Credentials: " + config.withCredentials);
      console.log(config);
    })
    /**
     * Request error handling
     * Available error params:
     * error.request
     * error.config
     */
    $axios.onError(error => {
      const originalRequest = error.config;
      const code = parseInt(error.response && error.response.status);
      console.log(error.response);

      /**
       * Unauthorized requests
       */
      if ([401, 403].includes(code) && !originalRequest._retry) {
        originalRequest._retry = true;
        let message = error.response.data.message ? error.response.data.message : null;
        /**
         * If a user makes a request and the X-CSFR-Token is invalid, go and get a new token
         * and try the same request again.
         */
        if(message && message.includes("X-CSRF-Token")) {
          // $axios.get(process.env.baseUrl + "/session/token?_format=json", { headers: { Authorization: null }}).then((res) => {
          //   localStorage.setItem('auth.csfr_token', res.data);
          //   Cookie.set('auth.csfr_token', res.data);
          //   // @TODO: das cookie und der local storage werden upgedatet, aber das Auth modul nimmt noch die alten werte
          //   return $axios(originalRequest);
          // })
        }
        /**
         * If someone still has a session in drupal and tries to login from frontend,
         * log out user from drupal first and try again original request.
         */
        else if (message && message.includes("This route can only be accessed by anonymous users")) {
          app.$auth.options.redirect.logout = '/user/login';
          //app.$auth.logout();
          return $axios(originalRequest);
        }
        else if (message && message.includes("This route can only be accessed by authenticated users.")) {
          return Promise.reject(error);
        }
        else {
          /**
           * Log out user, if anything is not working properly. If there is an error on
           * logout, just try it once.
           */
          if (error.config.url.indexOf('/user/logout') === -1) {
            app.$auth.options.redirect.logout = '/user/login';
            //app.$auth.logout();
            return Promise.reject(error);
          }
        }
    
      }


      /**
       * If a user tries to save the location and it is not locality, like DUBAI in United Arab Emirates 
       * there locality needs to be NULL, and City lies in administrative_area (Emirate in backend). So people 
       * could not save their profiles anymore, and also not register.
       * Test: Dubai, Bogota
       */
      if ( code === 422 && error.response.data.message.includes("field_user_location") && !originalRequest._retry ) {
        originalRequest._retry = true;
        if ( error.response.data.message.includes("Department field is not in the right format.") || 
          error.response.data.message.includes("administrative_area:  field must be blank.") || 
          error.response.data.message.includes("Province field is not in the right format.") || 
          error.response.data.message.includes("State field is not in the right format.")
          ) {
          originalRequest.data = originalRequest.data.replace('administrative_area', 'bk_aa');
        }
        else {  
          originalRequest.data = originalRequest.data.replace('locality', 'administrative_area');
        }
        return $axios(originalRequest);
      }

      return Promise.reject(error);
    });
  }