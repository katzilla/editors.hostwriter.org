import '@mdi/font/css/materialdesignicons.css';
import Vue from 'vue';
import Vuetify from 'vuetify';
let LRU = require("lru-cache");

const themeCache = new LRU({
  max: 10,
  maxAge: 1000 * 60 * 60 // 1 hour
})

Vue.use(
  Vuetify, {
    breakpoint: {
      thresholds: {
        xs: 480,
        sm: 768,
        md: (1024-16),
        lg: (1200-16),
        xl: (1200-16),
      },
    },
    theme: {
      primary: '#00005a',
      secondary: '#fe5621',
      accent: '#fe5621',
      error: '#c3003e',
      info: '#98a2b9',
      success: '#0fe076',
      warning: '#d2a400',
    },
    iconfont: 'mdi',
    options: {
      customProperties: true,
      themeCache,
      minifyTheme: function (css) {
        return process.env.NODE_ENV === 'production'
          ? css.replace(/[\s|\r\n|\r|\n]/g, '')
          : css
      }
    }
  }
);

