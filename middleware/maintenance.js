export default function({ store, redirect, context }) {
  // If the maintenance mode is activated, lead all requests to maintenance page
  if (process.env.MAINTENANCE_MODE === "true") {
    return redirect("/maintenance");
  }
}
