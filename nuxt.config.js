const pkg = require('./package');
require('dotenv').config();


module.exports = {
  mode: 'universal',
  env: {
    baseUrl: process.env.HOSTWRITER_API_BASEURL || 'http://127.0.0.1:3000'
    // serverBaseUrl: process.env.HOSTWRITER_API_BASEURL,
    // serverApiUrl: serverBaseUrl + '/jsonapi',
    // serverFilesUrl: process.env.HOSTWRITER_API_BASEURL
  },
  /*
  ** Faces hot reloading problem in safari and others (in dev mode)
  ** see: https://github.com/nuxt/nuxt.js/issues/3828
  ** and  https://github.com/vuejs/vue-cli/issues/1132
  */
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'development') {
      config
        .output
        .filename('[name].[hash].js')
        .end()
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: "Commission journalists worldwide - COVID-19 Collaboration Wire",
    titleTemplate: '%s',
    meta: [{
        hid: "description",
        name: "description",
        content: "Connect with trusted journalists from Hostwriters pool of 5000 vetted media professionals from 150+ countries, ready to fact-check, research and report."
      },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'msapplication-TileImage', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-144x144.png' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-navbutton-color', content: '#ffffff' },
      { name: 'application-name', content: 'Hostwriter' },
      { name: 'msapplication-tooltip', content: 'Hostwriter' },
      { name: 'msapplication-square70x70logo', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-70x70.png' },
      { name: 'msapplication-square144x144logo', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-144x144.png' },
      { name: 'msapplication-square150x150logo', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-150x150.png' },
      { name: 'msapplication-wide310x150logo', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-310x150.png' },
      { name: 'msapplication-square310x310logo', content: '/icons-touch-tiles/windows-tiles/_icons/win8-tile-310x310.png' },
      { name: 'apple-mobile-web-app-title', content: 'Hostwriter' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icons-touch-tiles/apple-touch/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/icons-touch-tiles/apple-touch/apple-touch-icon.png'},
      { rel: 'apple-touch-icon', sizes: '57x57', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-57x57.png'},
      { rel: 'apple-touch-icon', sizes: '72x72', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-72x72.png'},
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-76x76.png'},
      { rel: 'apple-touch-icon', sizes: '114x114', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-114x114.png'},
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-120x120.png'},
      { rel: 'apple-touch-icon', sizes: '144x144', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-144x144.png'},
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-152x152.png'},
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/icons-touch-tiles/apple-touch/apple-touch-icon-180x180.png'}
    ]
  },

  /**
   *
   * In case of global usage, You can set auth option to false in a specific component and the middleware will ignore that route.
   * This is needed by nuxt auth module: https://auth.nuxtjs.org/getting-started/middleware
   * export default {
   *   auth: false
   * }
   */
  router: {
    middleware: [
      'maintenance'  // custom maintenance mode
    ]
   },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    //'@mdi/font/css/materialdesignicons.css',
    //'material-design-icons-iconfont/dist/material-design-icons.css',
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vuetify',
    // see: https://axios.nuxtjs.org/extend
    '~/plugins/axios',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://github.com/Developmint/nuxt-purgecss
    'nuxt-purgecss',
        // Doc: https://github.com/pimlie/nuxt-matomo
    ['nuxt-matomo', {
      matomoUrl: '//tracking.hostwriter.org/',
      siteId: 7
    }],
  ],

  purgeCSS: {
    // your settings here
    whitelist: ['html', 'body'],
    // which classes *not* to remove
    whitelistPatterns: [/^_/, /^v-/, /^--v/, /^theme--/, /^elevation/, /^mdi/, /^material-icons/],
    paths: () => [
      './*.css',
      'components/**/*.vue',
      'layouts/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'node_modules/vuetify/src/**/*.js',
    ]
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.HOSTWRITER_API_BASEURL || 'http://127.0.0.1:3000',
    credentials: true
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {}
  }
}
